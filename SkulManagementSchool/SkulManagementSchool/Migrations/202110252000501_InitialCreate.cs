namespace SkulManagementSchool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Assessments",
                c => new
                    {
                        AssessmentId = c.Int(nullable: false, identity: true),
                        AssessmentName = c.String(),
                        Contributionpercent = c.Int(nullable: false),
                        TotalMarkAssessment = c.Int(nullable: false),
                        SubjectId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AssessmentId)
                .ForeignKey("dbo.Subjects", t => t.SubjectId)
                .Index(t => t.SubjectId);
            
            CreateTable(
                "dbo.LearnersAssessments",
                c => new
                    {
                        LearnersAssessmentId = c.Int(nullable: false, identity: true),
                        Mark = c.Int(nullable: false),
                        AssessmentId = c.Int(nullable: false),
                        LearnerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LearnersAssessmentId)
                .ForeignKey("dbo.Assessments", t => t.AssessmentId)
                .ForeignKey("dbo.Learners", t => t.LearnerId)
                .Index(t => t.AssessmentId)
                .Index(t => t.LearnerId);
            
            CreateTable(
                "dbo.Learners",
                c => new
                    {
                        LearnerId = c.Int(nullable: false, identity: true),
                        LearnerName = c.String(),
                        LearnerSurname = c.String(),
                        LearnerIdNumber = c.String(),
                        LearnerGrade = c.String(),
                        ParentId = c.Int(nullable: false),
                        ClassroomId = c.Int(),
                        LearnerCertifiedID = c.Binary(),
                        AcademicRecord = c.Binary(),
                    })
                .PrimaryKey(t => t.LearnerId)
                .ForeignKey("dbo.Parents", t => t.ParentId)
                .Index(t => t.ParentId);
            
            CreateTable(
                "dbo.Parents",
                c => new
                    {
                        ParentId = c.Int(nullable: false, identity: true),
                        ParentName = c.String(),
                        ParentSurname = c.String(),
                        ParentIdNumber = c.String(),
                        ParentOccupation = c.String(),
                        ParentCellNumber = c.String(),
                        ParentWorkNumber = c.String(),
                        ParentPhysicalAddress = c.String(),
                        ParentEmail = c.String(),
                        ParentRelationship = c.String(),
                        ParentCertifiedID = c.Binary(),
                        ProofOfAddress = c.Binary(),
                    })
                .PrimaryKey(t => t.ParentId);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        PaymentId = c.Int(nullable: false, identity: true),
                        ReferenceNumber = c.String(),
                        AmountPaid = c.String(),
                        LearnerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.PaymentId)
                .ForeignKey("dbo.Learners", t => t.LearnerId)
                .Index(t => t.LearnerId);
            
            CreateTable(
                "dbo.Subjects",
                c => new
                    {
                        SubjectId = c.Int(nullable: false, identity: true),
                        SubjectName = c.String(),
                        SubjectPassRequirement = c.String(),
                        ClassroomId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SubjectId)
                .ForeignKey("dbo.Classrooms", t => t.ClassroomId)
                .Index(t => t.ClassroomId);
            
            CreateTable(
                "dbo.Classrooms",
                c => new
                    {
                        ClassroomId = c.Int(nullable: false, identity: true),
                        ClassroomName = c.String(),
                        NumberofLearners = c.String(),
                        GradeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ClassroomId)
                .ForeignKey("dbo.Grades", t => t.GradeId)
                .Index(t => t.GradeId);
            
            CreateTable(
                "dbo.Grades",
                c => new
                    {
                        GradeId = c.Int(nullable: false, identity: true),
                        GradeName = c.String(),
                    })
                .PrimaryKey(t => t.GradeId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                        IdentityRole_Id = c.String(maxLength: 128),
                        IdentityUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.IdentityRole_Id)
                .ForeignKey("dbo.AspNetUsers", t => t.IdentityUser_Id)
                .Index(t => t.IdentityRole_Id)
                .Index(t => t.IdentityUser_Id);
            
            CreateTable(
                "dbo.Teachers",
                c => new
                    {
                        TeacherId = c.Int(nullable: false, identity: true),
                        TeacherName = c.String(),
                        TeacherSurname = c.String(),
                        TeacherGender = c.String(),
                        TeacherPreferedSubject = c.String(),
                    })
                .PrimaryKey(t => t.TeacherId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Email = c.String(),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IdentityUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                        IdentityUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.IdentityUser_Id)
                .Index(t => t.IdentityUser_Id);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        IdentityUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.LoginProvider, t.ProviderKey })
                .ForeignKey("dbo.AspNetUsers", t => t.IdentityUser_Id)
                .Index(t => t.IdentityUser_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "IdentityUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "IdentityUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.IdentityUserClaims", "IdentityUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "IdentityRole_Id", "dbo.AspNetRoles");
            DropForeignKey("dbo.Assessments", "SubjectId", "dbo.Subjects");
            DropForeignKey("dbo.Subjects", "ClassroomId", "dbo.Classrooms");
            DropForeignKey("dbo.Classrooms", "GradeId", "dbo.Grades");
            DropForeignKey("dbo.LearnersAssessments", "LearnerId", "dbo.Learners");
            DropForeignKey("dbo.Payments", "LearnerId", "dbo.Learners");
            DropForeignKey("dbo.Learners", "ParentId", "dbo.Parents");
            DropForeignKey("dbo.LearnersAssessments", "AssessmentId", "dbo.Assessments");
            DropIndex("dbo.AspNetUserLogins", new[] { "IdentityUser_Id" });
            DropIndex("dbo.IdentityUserClaims", new[] { "IdentityUser_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "IdentityUser_Id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "IdentityRole_Id" });
            DropIndex("dbo.Classrooms", new[] { "GradeId" });
            DropIndex("dbo.Subjects", new[] { "ClassroomId" });
            DropIndex("dbo.Payments", new[] { "LearnerId" });
            DropIndex("dbo.Learners", new[] { "ParentId" });
            DropIndex("dbo.LearnersAssessments", new[] { "LearnerId" });
            DropIndex("dbo.LearnersAssessments", new[] { "AssessmentId" });
            DropIndex("dbo.Assessments", new[] { "SubjectId" });
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.IdentityUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Teachers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Grades");
            DropTable("dbo.Classrooms");
            DropTable("dbo.Subjects");
            DropTable("dbo.Payments");
            DropTable("dbo.Parents");
            DropTable("dbo.Learners");
            DropTable("dbo.LearnersAssessments");
            DropTable("dbo.Assessments");
        }
    }
}
