﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SkulManagementSchool.Startup))]
namespace SkulManagementSchool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
