﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SkulManagementSchool.Models;

namespace SkulManagementSchool.Controllers
{
    public class LearnersController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Learners
        public ActionResult Index()
        {
            var learnerList = db.Learners.ToList();
            var allocatedLearnerList = new List<LearnerList>();
            foreach(var item in learnerList)
            {
                var allocatedLearners = new LearnerList();
                allocatedLearners.LearnerId = item.LearnerId;
                allocatedLearners.LearnerName = item.LearnerName;
                allocatedLearners.LearnerSurname = item.LearnerSurname;
                allocatedLearners.LearnerIdNumber = item.LearnerIdNumber;
                if (item.ClassroomId == 0)
                {
                    allocatedLearners.ClassroomName = "Not Allocated";
                }
                else
                {
                    allocatedLearners.ClassroomName = db.Classrooms.Where(p => p.ClassroomId == item.ClassroomId).Select(k => k.ClassroomName).FirstOrDefault();
                }
                allocatedLearnerList.Add(allocatedLearners);
            }

            return View(allocatedLearnerList);
        }

        // GET: Learners/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Learner learner = db.Learners.Find(id);
            if (learner == null)
            {
                return HttpNotFound();
            }
            return View(learner);
        }

        // GET: Learners/Create
        public ActionResult Create()
        {
            return View();
        } 
        
        public ActionResult AddLearner()
        {
            return View();
        }

        // POST: Learners/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LearnerId,LearnerName,LearnerSurname,LearnerIDnumber,LearnerGrade")] Learner learner)
        {
            if (ModelState.IsValid)
            {
                db.Learners.Add(learner);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(learner);
        }

        // GET: Learners/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Learner learner = db.Learners.Find(id);
            if (learner == null)
            {
                return HttpNotFound();
            }
            return View(learner);
        }
        [Authorize(Roles = "admin")]
        public ActionResult AllocateLearner(int? id)
        {
            ViewBag.ClassroomId = new SelectList(db.Classrooms, "ClassroomId", "ClassroomName");
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Learner learner = db.Learners.Find(id);
            if (learner == null)
            {
                return HttpNotFound();
            }
            return View(learner);
        }

        [HttpPost]
        [Authorize(Roles = "Parent")]
        public JsonResult SaveDetails(RegisterParent register)
        {
            Learner learner = new Learner();
            learner.LearnerName = register.LearnerName;
            learner.LearnerSurname = register.LearnerSurname;
            learner.LearnerIdNumber = register.LearnerIdNumber;
            learner.LearnerGrade = register.LearnerGrade;
            learner.ClassroomId = 0;
            var parentId = db.Parents.Where(u => u.ParentEmail == User.Identity.Name).Select(k => k.ParentId).FirstOrDefault();
            learner.ParentId = parentId;
            db.Learners.Add(learner);
            db.SaveChanges();
            if (learner.LearnerId > 0)
            {
                return new JsonResult { Data = new { status = true, url = Url.Action("SaveDocuments", new { id = learner.LearnerId }) } };
            }
            return new JsonResult { Data = new { status = false } };
        }
        [Authorize(Roles = "Parent")]

        public ActionResult SaveDocuments(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegisterViewModel regDocs = new RegisterViewModel();
            regDocs.LearnerId = (int)id;

            return View(regDocs);
        }

        [HttpPost]
        public ActionResult SaveDocuments([Bind(Exclude = "LearnerCertifiedID,AcademicRecord")] RegisterViewModel regDocs)
        {
            var learnerDocs = db.Learners.Where(m => m.LearnerId == regDocs.LearnerId).FirstOrDefault();
            try
            {
                learnerDocs.LearnerCertifiedID = getFileById("LearnerCertifiedID");
                learnerDocs.AcademicRecord = getFileById("AcademicRecord");

                db.Entry(learnerDocs).State = EntityState.Modified;
                db.SaveChanges();
                RegisterViewModel registerModel = new RegisterViewModel();
                registerModel.ParentId = regDocs.ParentId;

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                ModelState.AddModelError("" + e.ToString(), "Unable to save changes. " +
                 "Try again, and if the problem persists see your system administrator.");


                return View(regDocs);
            }
        }

        // POST: Learners/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LearnerId,LearnerName,LearnerSurname,LearnerIDnumber,LearnerGrade")] Learner learner)
        {
            if (ModelState.IsValid)
            {
                db.Entry(learner).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(learner);
        }

        [HttpPost]
        [Authorize(Roles ="admin")]
        public ActionResult AllocateLearner(Learner learner)
        {
            if (ModelState.IsValid)
            {
                db.Entry(learner).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(learner);
        }

        // GET: Learners/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Learner learner = db.Learners.Find(id);
            if (learner == null)
            {
                return HttpNotFound();
            }
            return View(learner);
        }

        // POST: Learners/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Learner learner = db.Learners.Find(id);
            db.Learners.Remove(learner);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private byte[] getFileById(string id)
        {
            byte[] fileData = null;
            HttpPostedFileBase poImgFile = Request.Files[id];

            using (var binary = new BinaryReader(poImgFile.InputStream))
            {
                fileData = binary.ReadBytes(poImgFile.ContentLength);
            }

            return fileData;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
