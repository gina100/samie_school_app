﻿using SkulManagementSchool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SkulManagementSchool.Controllers
{
    public class ReportsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LearnerReport(int learnerId)
        {
            var report = new Report();
            report.LearnerName = db.Learners.Where(l => l.LearnerId == learnerId).Select(p => p.LearnerName).FirstOrDefault();
            var classroomId= db.Learners.Where(l => l.LearnerId == learnerId).Select(p => p.ClassroomId).FirstOrDefault();
            report.ClassName = db.Classrooms.Where(p => p.ClassroomId == classroomId).Select(o => o.ClassroomName).FirstOrDefault();
            report.Year = 2021;
            var subjects = db.Subjects.Where(p => p.ClassroomId == classroomId).ToList();
            var numberOfLearners = db.Learners.Where(p => p.ClassroomId == classroomId).Count();
            var subjectLists = new List<SubjectList>();
            foreach(var item in subjects)
            {
                var subObj = new SubjectList();
                subObj.SubjectName = item.SubjectName;
                subObj.Marks = GetLearnerSubjectMark(item.SubjectId, learnerId);
                subObj.Percentage = subObj.Marks;
                subObj.GradeAverage = (GetTotalLearnersSubjectMarks(item.SubjectId, classroomId) / numberOfLearners);
                subjectLists.Add(subObj);
            }
            report.subjectLists = subjectLists;
            return View(report);
        }

        private int GetTotalLearnersSubjectMarks(int subjectId,int? classroomId)
        {
            var learners = db.Learners.Where(p => p.ClassroomId == classroomId).ToList();
            var totalLearnerSubjectMark = 0;
            foreach (var learner in learners)
            {
                totalLearnerSubjectMark += GetLearnerSubjectMark(subjectId,learner.LearnerId);
            }

            return totalLearnerSubjectMark;
        }

        private int GetLearnerSubjectMark(int subjectId, int learnerId)
        {
             var subjectAssessments = db.Assessments.Where(p => p.SubjectId == subjectId).ToList();
             var learnerSubjectMark = 0;
                
            if (subjectAssessments != null)
                {
                    foreach (var assessmentItem in subjectAssessments)
                    {
                        var learnerAssessmentMark = db.LearnersAssessments.Where(k => k.LearnerId == learnerId && k.AssessmentId == assessmentItem.AssessmentId).Select(u => u.Mark).FirstOrDefault();
                        learnerSubjectMark =learnerSubjectMark+ (int)(((double)learnerAssessmentMark / (double)assessmentItem.TotalMarkAssessment) * (double)assessmentItem.Contributionpercent);
                    }
                }

            return learnerSubjectMark;
        }
    }
}