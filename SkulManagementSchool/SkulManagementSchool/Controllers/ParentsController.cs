﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SkulManagementSchool.Models;

namespace SkulManagementSchool.Controllers
{
    public class ParentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Parents
        public ActionResult Index()
        {
            return View(db.Parents.ToList());
        }

        // GET: Parents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Parent parent = db.Parents.Find(id);
            if (parent == null)
            {
                return HttpNotFound();
            }
            return View(parent);
        }

        // GET: Parents/Create
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public JsonResult SaveDetails(RegisterParent register)
        {
            Learner learner = new Learner();
            learner.LearnerName = register.LearnerName;
            learner.LearnerSurname = register.LearnerSurname;
            learner.LearnerIdNumber = register.LearnerIdNumber;
            learner.LearnerGrade = register.LearnerGrade;
            Parent parent = new Parent();
            parent.ParentName = register.ParentName;
            parent.ParentSurname = register.ParentSurname;
            parent.ParentIdNumber = register.ParentIdNumber;
            parent.ParentOccupation = register.ParentOccupation;
            parent.ParentCellNumber = register.ParentCellNumber;
            parent.ParentWorkNumber = register.ParentWorkNumber;
            parent.ParentPhysicalAddress = register.ParentPhysicalAddress;
            parent.ParentRelationship = register.ParentRelationship;

            db.Parents.Add(parent);            
            db.SaveChanges();
            learner.ParentId = parent.ParentId;
            db.Learners.Add(learner);
            db.SaveChanges();
            if (parent.ParentId> 0)
            {
                return new JsonResult { Data = new { status = true, url = Url.Action("SaveDocuments", new { id =parent.ParentId }) } };
            }
            return new JsonResult { Data = new { status = false } };
        }


        public ActionResult SaveDocuments(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegisterViewModel regDocs= new RegisterViewModel();
            regDocs.ParentId = (int)id;

            return View(regDocs);
        }

        [HttpPost]
        public ActionResult SaveDocuments([Bind(Exclude = "ParentCertifiedID,ProofOfAddress,LearnerCertifiedID,AcademicRecord")] RegisterViewModel regDocs)
        {
            var parentDocs = db.Parents.Where(m => m.ParentId == regDocs.ParentId).FirstOrDefault();
            var learnerDocs = db.Learners.Where(m => m.ParentId == regDocs.ParentId).FirstOrDefault();
            try
            {
                parentDocs.ParentCertifiedID= getFileById("ParentCertifiedID");
                parentDocs.ProofOfAddress = getFileById("ProofOfAddress");
                learnerDocs.LearnerCertifiedID = getFileById("LearnerCertifiedID");
                learnerDocs.AcademicRecord = getFileById("AcademicRecord");

                db.Entry(parentDocs).State = EntityState.Modified;
                db.SaveChanges();
                db.Entry(learnerDocs).State = EntityState.Modified;
                db.SaveChanges();
                RegisterViewModel registerModel = new RegisterViewModel();
                registerModel.ParentId = regDocs.ParentId;

                return RedirectToAction("CreateAccount", registerModel);
            }
            catch (Exception e)
            {
                ModelState.AddModelError("" + e.ToString(), "Unable to save changes. " +
                 "Try again, and if the problem persists see your system administrator.");


                return View(regDocs);
            }
        }
        // POST: Parents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ParentId,ParentName,ParentSurname,ParentIDNumber,ParentOccupation,ParentCellNumber,ParentWorkNumber,ParentPhysicalAddress,ParentRelationship")] Parent parent)
        {
            if (ModelState.IsValid)
            {
                db.Parents.Add(parent);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(parent);
        }

        public ActionResult CreateAccount(RegisterViewModel registerModel)
        {
            return View(registerModel);
        }
        // GET: Parents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Parent parent = db.Parents.Find(id);
            if (parent == null)
            {
                return HttpNotFound();
            }
            return View(parent);
        }

        // POST: Parents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ParentId,ParentName,ParentSurname,ParentIDNumber,ParentOccupation,ParentCellNumber,ParentWorkNumber,ParentPhysicalAddress,ParentRelationship")] Parent parent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(parent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(parent);
        }

        // GET: Parents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Parent parent = db.Parents.Find(id);
            if (parent == null)
            {
                return HttpNotFound();
            }
            return View(parent);
        }

        // POST: Parents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Parent parent = db.Parents.Find(id);
            db.Parents.Remove(parent);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        private byte[] getFileById(string id)
        {
            byte[] fileData = null;
            HttpPostedFileBase poImgFile = Request.Files[id];

            using (var binary = new BinaryReader(poImgFile.InputStream))
            {
                fileData = binary.ReadBytes(poImgFile.ContentLength);
            }

            return fileData;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
