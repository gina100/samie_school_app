﻿using SkulManagementSchool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SkulManagementSchool.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            var parentName = db.Parents.Where(u => u.ParentEmail == User.Identity.Name).Select(k => k.ParentName).FirstOrDefault();
            var learnersTot = db.Learners.Count();
            ViewBag.ParentName = parentName;
            ViewBag.learnerTot = learnersTot;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}