﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SkulManagementSchool.Models;

namespace SkulManagementSchool.Controllers
{
    public class LearnersAssessmentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: LearnersAssessments
        public ActionResult Index()
        {
            var learnersAssessments = db.LearnersAssessments.Include(l => l.Assessment).Include(l => l.Learner);
            return View(learnersAssessments.ToList());
        }

        public ActionResult MarksCaptured(int assessmentId, string className,string subjectName)
        {
            ViewBag.ClassName = className;
            ViewBag.SubjectName = subjectName;
            var learnersAssessments = db.LearnersAssessments.Include(l => l.Assessment).Include(l => l.Learner).Where(l=>l.AssessmentId==assessmentId);
            ViewBag.AssessmentName = learnersAssessments.FirstOrDefault().Assessment.AssessmentName;

            return View(learnersAssessments.ToList());
        }

        // GET: LearnersAssessments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LearnersAssessment learnersAssessment = db.LearnersAssessments.Find(id);
            if (learnersAssessment == null)
            {
                return HttpNotFound();
            }
            return View(learnersAssessment);
        }

        // GET: LearnersAssessments/Create
        public ActionResult Create()
        {
            ViewBag.AssessmentId = new SelectList(db.Assessments, "AssessmentId", "AssessmentName");
            ViewBag.LearnerId = new SelectList(db.Learners, "LearnerId", "LearnerName");
            return View();
        }

        public ActionResult CaptureMarks(int assessmentId,int classroomId)
        {
            var classList = db.Learners.Where(p => p.ClassroomId == classroomId).ToList();
            var learnerMarksList = new List<LearnersAssessment>();

            foreach(var item in classList)
            {
                var la = new LearnersAssessment();
                la.AssessmentId = assessmentId;
                la.Learner = item;
                la.LearnerId = item.LearnerId;
                la.Mark = 0;
                learnerMarksList.Add(la);
            }
            return View(learnerMarksList);
        }

        [HttpPost]
        public ActionResult CaptureMarks(List<LearnersAssessment> learnersAssessments)
        {
            foreach(var item in learnersAssessments)
            {
                var tot = db.Assessments.Where(k => k.AssessmentId == item.AssessmentId).Select(l => l.TotalMarkAssessment).FirstOrDefault();
                var learner = db.Learners.Where(n => n.LearnerId == item.LearnerId).FirstOrDefault();
                if (item.Mark > tot)
                {
                    item.Learner = learner;
                    ModelState.AddModelError("", "Unable to save Marks Cannot Be greater " + tot + " Check " + learner.LearnerName + "'s Mark");
                    return View(learnersAssessments);
                }
            }

            foreach (var item in learnersAssessments)
            {
                var learnerAssessment = db.LearnersAssessments.Where(p => p.AssessmentId == item.AssessmentId && p.LearnerId == item.LearnerId).FirstOrDefault();
                if (learnerAssessment != null)
                {
                    learnerAssessment.Mark = item.Mark;
                    db.Entry(learnerAssessment).State = EntityState.Modified;
                }
                else
                {
                    db.LearnersAssessments.Add(item);
                }

                db.SaveChanges();
            }
            var assessmentId = learnersAssessments.FirstOrDefault().AssessmentId;
            var subjectId = db.Assessments.Where(l => l.AssessmentId == assessmentId).Select(p => p.SubjectId).FirstOrDefault();

            return RedirectToAction("SubjectIndex","Assessments",new { subjectId=subjectId});
        }

        // POST: LearnersAssessments/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "LearnersAssessmentId,Mark,AssessmentId,LearnerId")] LearnersAssessment learnersAssessment)
        {
            if (ModelState.IsValid)
            {
                db.LearnersAssessments.Add(learnersAssessment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.AssessmentId = new SelectList(db.Assessments, "AssessmentId", "AssessmentName", learnersAssessment.AssessmentId);
            ViewBag.LearnerId = new SelectList(db.Learners, "LearnerId", "LearnerName", learnersAssessment.LearnerId);
            return View(learnersAssessment);
        }

        // GET: LearnersAssessments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LearnersAssessment learnersAssessment = db.LearnersAssessments.Find(id);
            if (learnersAssessment == null)
            {
                return HttpNotFound();
            }
            ViewBag.AssessmentId = new SelectList(db.Assessments, "AssessmentId", "AssessmentName", learnersAssessment.AssessmentId);
            ViewBag.LearnerId = new SelectList(db.Learners, "LearnerId", "LearnerName", learnersAssessment.LearnerId);
            return View(learnersAssessment);
        }

        // POST: LearnersAssessments/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "LearnersAssessmentId,Mark,AssessmentId,LearnerId")] LearnersAssessment learnersAssessment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(learnersAssessment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AssessmentId = new SelectList(db.Assessments, "AssessmentId", "AssessmentName", learnersAssessment.AssessmentId);
            ViewBag.LearnerId = new SelectList(db.Learners, "LearnerId", "LearnerName", learnersAssessment.LearnerId);
            return View(learnersAssessment);
        }

        // GET: LearnersAssessments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LearnersAssessment learnersAssessment = db.LearnersAssessments.Find(id);
            if (learnersAssessment == null)
            {
                return HttpNotFound();
            }
            return View(learnersAssessment);
        }

        // POST: LearnersAssessments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            LearnersAssessment learnersAssessment = db.LearnersAssessments.Find(id);
            db.LearnersAssessments.Remove(learnersAssessment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
