﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SkulManagementSchool.Models
{
    public class Classroom
    {
        [Key]
        public int ClassroomId { get; set; }
        [Display (Name =  "Classroom Name" )]
        public string ClassroomName { get; set; }
        [Display(Name = "Number of Learners")]
        public string NumberofLearners { get; set; }
        [Display(Name = "Grade")]
        [ForeignKey("Grade")]
        public int GradeId { get; set; }
        public virtual Grade Grade { get; set; }
        //public ICollection<Learner> Learners { get; set; }
        //.
        //public ICollection<Classroom> Classrooms { get; set; }
    }
}