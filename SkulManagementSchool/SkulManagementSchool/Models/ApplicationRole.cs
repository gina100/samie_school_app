﻿using Microsoft.AspNet.Identity.EntityFramework;


namespace SkulManagementSchool.Models
{
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole()
        {

        }

        public ApplicationRole(string roleName)
            : base(roleName)
        {
        }
    }
}