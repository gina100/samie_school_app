﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SkulManagementSchool.Models
{
    public class LearnerList
    {
        public int LearnerId { get; set; }
        [Display(Name = "Name")]
        public string LearnerName { get; set; }
        [Display(Name = "Surname")]
        public string LearnerSurname { get; set; }
        [Display(Name = "ID Number")]
        public string LearnerIdNumber { get; set; }
       
        [Display(Name = "Allocated Classroom")]
        public string ClassroomName{ get; set; }
       
    }
}