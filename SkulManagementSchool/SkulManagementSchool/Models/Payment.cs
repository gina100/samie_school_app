﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SkulManagementSchool.Models
{
    public class Payment
    {
        [Key]
        public int PaymentId { get; set; }
        [Display (Name = "Reference Number")]
        public string ReferenceNumber { get; set; }
        [Display(Name = "Amount Paid")]
        public int TotAmount  { get; set; }
        [Display(Name = "Learner")]
        //[ForeignKey ("Learner")]
        public int LearnerId { get; set; }
        //public virtual Learner Learner { get; set; }

    }
}