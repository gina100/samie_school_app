﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SkulManagementSchool.Models
{
    public class LearnersAssessment
    {
        [Key]
        public int LearnersAssessmentId { get; set; }
        [Display(Name = "Mark")]
        public int Mark { get; set; }
        [Display(Name = "Assessment")]
        [ForeignKey("Assessment")]
        public int AssessmentId { get; set; }
        [ForeignKey("Learner")]
        public int LearnerId { get; set; }
        public virtual Learner Learner { get; set; }
        public virtual Assessment Assessment { get; set; }
    }
}