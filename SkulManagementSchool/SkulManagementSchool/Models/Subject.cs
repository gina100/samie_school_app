﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SkulManagementSchool.Models
{
    public class Subject
    {
        [Key]
        public int SubjectId { get; set; }
        [Display (Name = "Subject Name")]
        public string SubjectName { get; set; }
        [Display (Name = "Pass Requirement")]
        public string SubjectPassRequirement { get; set; }
        [Display(Name = "Classroom")]
        [ForeignKey ("Classroom")]
        public int ClassroomId { get; set; }
        public virtual Classroom Classroom { get; set; }
        public ICollection<Assessment> Assessments { get; set; }
    }
}