﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SkulManagementSchool.Models
{
    public class Grade
    {
        [Key]
        public int GradeId { get; set; }
        [Display (Name = "Grade Name")]
        public string GradeName { get; set; }
        public ICollection<Classroom> Classrooms { get; set; }
    }
}