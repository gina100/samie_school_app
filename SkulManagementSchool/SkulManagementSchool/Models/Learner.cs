﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SkulManagementSchool.Models
{
    public class Learner
    {
        [Key]
        public int LearnerId { get; set; }
        [Display(Name = "Name")]
        public string LearnerName { get; set; }
        [Display(Name = "Surname")]
        public string LearnerSurname { get; set; }
        [Display(Name = "ID Number")]
        public string LearnerIdNumber { get; set; }
        [Display(Name = "Grade")]
        public string LearnerGrade { get; set; }
        [Display(Name = "Parent")]
        [ForeignKey("Parent")]
        public int ParentId { get; set; }
        [Display(Name = "ClassroomId")]
        ///[ForeignKey("Classroom")]
        public int? ClassroomId { get; set; }
        public virtual Parent Parent { get; set; }
        //public virtual Classroom Classroom { get; set; }
        public ICollection<LearnersAssessment> LearnersAssessments { get; set; }
        public ICollection<Payment> Payments { get; set; }
        public byte[] LearnerCertifiedID { get; set; }
        public byte[] AcademicRecord { get; set; }
    }
}