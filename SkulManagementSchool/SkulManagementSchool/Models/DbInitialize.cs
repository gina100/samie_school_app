﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SkulManagementSchool.Models
{

    public class DbInitialize<T> : DropCreateDatabaseIfModelChanges<IdentityDbContext>
    {
        protected override void Seed(IdentityDbContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new
                                            UserStore<ApplicationUser>(context));

            var roleManager = new RoleManager<ApplicationRole>(new
                                      RoleStore<ApplicationRole>(context));

            const string name = "samie@gmail.com";
            const string rol = "admin";
            const string password = "#2021Password";

            if (!roleManager.RoleExists(rol))
            {
                var roleresult = roleManager.Create(new ApplicationRole(rol));
            }

            if (!roleManager.RoleExists("Parent"))
            {
                var roleresult = roleManager.Create(new ApplicationRole("Parent"));
            }

            var userAdmin = new ApplicationUser();
            var userParent = new ApplicationUser();
            var db = new ApplicationDbContext();
            var parent = new Parent();

            userAdmin.UserName = name;
            // userAdmin.userStatus = "No";
            userParent.UserName = "parent@gmail.com";

            var adminresult = userManager.Create(userAdmin, password);
            var parentResult = userManager.Create(userParent, password);


            if (adminresult.Succeeded)
            {
                var result = userManager.AddToRole(userAdmin.Id, rol);
            }
            if (parentResult.Succeeded)
            {
                var result = userManager.AddToRole(userParent.Id, "Parent");
                parent.ParentName = "Samukelo";
                parent.ParentSurname = "Dindi";
                parent.ParentIdNumber = "7501020128087";
                parent.ParentOccupation = "Teacher";
                parent.ParentCellNumber = "0827959051";
                parent.ParentWorkNumber = "0315543647";
                parent.ParentPhysicalAddress = "21 Dronfield Road";
                parent.ParentEmail = "parent@gmail.com";
                parent.ParentRelationship = "Father";
                db.Parents.Add(parent);
                db.SaveChanges();
                //customer.CustomerEmail = userCustomer.UserName;
                //customer.CustomerName = "Thandeka";
                //customer.CustomerPlace = "Durban";
                //customer.CustomerGender = "Female";
                //customer.CustomerOccupation = "Student";
                //customer.CustomerMobileNumber = "0653066402";
                //customer.CustomerDateOfBirth = new DateTime(1999, 04, 11);
                //db.Customers.Add(customer);
                //db.SaveChanges();
            }

            base.Seed(context);
        }
       }
    }