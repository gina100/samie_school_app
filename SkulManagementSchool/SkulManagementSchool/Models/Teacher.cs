﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SkulManagementSchool.Models
{
    public class Teacher
    {
        [Key]
        public int TeacherId { get; set; }
        [Display(Name = "Name")]
        public string TeacherName { get; set; }
        [Display(Name = "Surname")]
        public string TeacherSurname { get; set; }
        [Display(Name = "Gender")]
        public string TeacherGender { get; set; }
        [Display(Name = "Prefered Subject")]
        public string TeacherPreferedSubject { get; set; }

    }
}