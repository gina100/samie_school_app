﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SkulManagementSchool.Models
{
    public class Parent
    {
        [Key]
        public int ParentId { get; set; }
        [Display(Name = "Name")]
        public string ParentName { get; set; }
        [Display(Name = "Surname")]
        public string ParentSurname { get; set; }
        [Display(Name = "ID Number")]
        public string ParentIdNumber { get; set; }
        [Display(Name = "Occupation")]
        public string ParentOccupation { get; set; }
        [Display(Name = "Cellphone Number")]
        public string ParentCellNumber { get; set; }
        [Display(Name = "Work Number")]
        public string ParentWorkNumber { get; set; }
        [Display(Name = "Physical Address")]
        public string ParentPhysicalAddress { get; set; }
        [Display(Name ="Email")]
        public string ParentEmail { get; set; }
        [Display(Name = "Relationship")]
        public string ParentRelationship { get; set; }
        public ICollection<Learner> Learners { get; set; }
        
        public byte[] ParentCertifiedID { get; set; }
        public byte[] ProofOfAddress { get; set; }
    }
}
