﻿using System.Collections.Generic;


namespace SkulManagementSchool.Models
{
    public class Report
    {
        public string LearnerName { get; set; }
        public string ClassName { get; set; }
        public int Year { get; set; }
        public List<SubjectList> subjectLists { get; set; }
        public string Comment { get; set; }
    }

    public class SubjectList
    {
        public string SubjectName { get; set; }
        public int Marks { get; set; }
        public int Percentage { get; set; }
        public int GradeAverage{ get; set; }
    }
}