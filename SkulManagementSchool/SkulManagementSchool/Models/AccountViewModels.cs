﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SkulManagementSchool.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Parent Name")]
        public string ParentName { get; set; }
        [Display(Name = "Parent Surname")]
        public string ParentSurname { get; set; }
        [Display(Name = "Parent ID Number")]
        public string ParentIdNumber { get; set; }
        public int ParentId { get; set; }
        public int LearnerId { get; set; }
        [Display(Name = "Occupation")]
        public string ParentOccupation { get; set; }
        [Display(Name = "Cellphone Number")]
        public string ParentCellNumber { get; set; }
        [Display(Name = "Work Number")]
        public string ParentWorkNumber { get; set; }
        [Display(Name = "Physical Address")]
        public string ParentPhysicalAddress { get; set; }
        [Display(Name = "Relationship")]
        public string ParentRelationship { get; set; }
        [Display(Name = "Parent Certified Copy of ID")]
        public byte[] ParentCertifiedID { get; set; }
        [Display(Name = "Proof of Address")]
        public byte[] ProofOfAddress { get; set; }
        [Display(Name = "Learner Name")]
        public string LearnerName { get; set; }
        [Display(Name = "Learner Surname")]
        public string LearnerSurname { get; set; }
        [Display(Name = "Leaner ID Number")]
        public string LearnerIdNumber { get; set; }
        [Display(Name = "Grade")]
        public string LearnerGrade { get; set; }
        [Display(Name = "Learner Certified Copy of ID/ Certificate")]
        public byte[] LearnerCertifiedID { get; set; }
        [Display(Name = "Academic Record")]
        public byte[] AcademicRecord { get; set; }
    }
    public class RegisterParent
    {
        [Display(Name = "Parent Name")]
        public string ParentName { get; set; }
        [Display(Name = "Parent Surname")]
        public string ParentSurname { get; set; }
        [Display(Name = "Parent ID Number")]
        public string ParentIdNumber { get; set; }
        public int ParentId { get; set; }
        [Display(Name = "Occupation")]
        public string ParentOccupation { get; set; }
        [Display(Name = "Cellphone Number")]
        public string ParentCellNumber { get; set; }
        [Display(Name = "Work Number")]
        public string ParentWorkNumber { get; set; }
        [Display(Name = "Physical Address")]
        public string ParentPhysicalAddress { get; set; }
        [Display(Name = "Relationship")]
        public string ParentRelationship { get; set; }
        [Display(Name = "Parent Certified Copy of ID")]
        public byte[] ParentCertifiedID { get; set; }
        [Display(Name = "Proof of Address")]
        public byte[] ProofOfAddress { get; set; }
        [Display(Name = "Learner Name")]
        public string LearnerName { get; set; }
        [Display(Name = "Learner Surname")]
        public string LearnerSurname { get; set; }
        [Display(Name = "Leaner ID Number")]
        public string LearnerIdNumber { get; set; }
        [Display(Name = "Grade")]
        public string LearnerGrade { get; set; }
        [Display(Name = "Learner Certified Copy of ID/ Certificate")]
        public byte[] LearnerCertifiedID { get; set; }
        [Display(Name = "Academic Record")]
        public byte[] AcademicRecord { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
