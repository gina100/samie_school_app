﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SkulManagementSchool.Models
{
    public class Assessment
    {
        [Key]
        public int AssessmentId { get; set; }
        [Display(Name = " Assessment Name")]
        public string AssessmentName { get; set; }
        [Display(Name = "Contribution percent")]
        public int Contributionpercent { get; set; }
        [Display(Name = "Total Mark Assessment")]
        public int TotalMarkAssessment { get; set; }
        [Display(Name = "Subject")]
        [ForeignKey("Subject")]
        public int SubjectId { get; set; }

        public virtual Subject Subject { get; set; }
        public ICollection<LearnersAssessment> LearnersAssessments { get; set; }

    }
}